NAME:: Mags Maenad
TITLE:: Cowbird Seeds
WEB:: https://twitter.com/MaenadMags
Select a mystery seed and plant it.

Celebrate and name it when you notice it first poking above the soil.

Photograph it every day and compare your photos. How would you know if an imposter took the place of this thing you’ve nurtured?
%%
NAME:: Jonathan Dersch
TITLE:: Plant Dungeon Generator
WEB:: https://jdersch.itch.io/
Find a plant, take a picture, map it. Branches & stems are hallways, leafs are rooms. Fill rooms with bugs (d6+d6):

1 Tired
2 Hungry
3 Afraid
4 Lonely
5 Angry
6 Confident
+
1 Aphids
2 Ants
3 Beetle
4 Bee
5 Spider
6 Wasp

One has stolen the Royal Jelly. All desire it.
%%
NAME:: NotAPipe
TITLE:: Shopkeeper
WEB:: https://notapipe.itch.io
You are a village shopkeeper. Sketch map of area and your shop. Roll d6. You sell 1. plant 2. medicinal herb 3. liquid 4. mineral 5. scent 6. artifact. Sketch item. Log: Describe a customer, a price, did you haggle? What will they use it for? Repeat until you close for the day.
%%
NAME:: Marc Strocks
TITLE:: Bounty Hunter
WEB:: https://twitter.com/spacesibling
You explore space w/ a Ship, Gun & Wrench
Draw a Triple Venn Diagram
Label circles: [S] [G] [W]
Make 1 bigger
To enter a new place, drop a coin
Where it lands the challenge is
S=chase
G=fight
W=tech

To use skills, try to aim where it drops
S=fast
G=strong
W=smart
Best at: big ◯
%%
NAME:: Kris Keillor
TITLE:: Second World Zero
WEB:: https://shsys.itch.io/
A Crewmate, Cyborg, & Shipmate crew an evac ship.

The Crewmate & Cyborg have Mind & Body qualities: D12.
The Shipmate has Compute & Build drones: D12.

Use your cache of 20 Hydro, 15 Atmo, & 30 Litho on Tasks.
Failing a risky Task drops your die one size.
Find safe haven.
%%
NAME:: KiKi
TITLE:: Scrapbook
WEB:: http://battlefluff.carrd.co
You will need: journal, 2d6, adhesive
Roll the dice, die 1 is quantity (double the result), die 2 is colour
1. Green
2. Yellow
3. Orange
4. Red
5. Brown
6. White
Go and collect pieces of plant/other natural debris in colour and quantity you rolled. Stick them in journal. Repeat.
%%
NAME:: lectronice
TITLE:: Paper Tron
WEB:: https://merveilles.town/@ice
Draw the arena on ⌗ paper.

┃ are obstacles.
△ let you jump above the next point.
◯ teleport you to other ◯

Choose a color and a starting point. On your turn, add 3 segments to the end of your own ┃

Lose if you run into a ┃ (including yours)
Win if you're the last.
%%
NAME:: Ian Mancuso & Milo Newman
TITLE:: dOrganisms
WEB:: https://twitter.com/BloodOrangeSt
Start with # of players
1 is judge
Judge makes list of X organisms
Players roll set # of dX
# rolled represents organism
Organism is located where its die lands
Organisms fight/help others near them
Players describe fights
Judge decides victor
Last player standing is new judge
%%
NAME:: Maxwell Dolor
TITLE:: Oil and Land
WEB:: https://twitter.com/MaxTheWZRD
Pick Oil or Land
If you pick Oil you have a +1 to actions of machinery
If you pick Land you have a +1 to actions of nature

When you take an action your Addition must be higher than the DC set by the GM
After you take an action you gain a cumulative +1 to the next action you take
%%
NAME:: Aaron Goss
TITLE:: Tweetville
WEB:: https://aarongoss.itch.io/
Let's make a happy little bird village together! Invite birds (🦢🦜🦚🦉 etc), place perches (🏡🚘🪑🎩 etc), and add toys (🪀📚🎈👓 etc) to Tweetville by replying to this tweet (http://tiny.cc/fa23tz) with emojis for them.

Every 💬,🔃, and ❤ your bird has is a new friend for it.
%%
NAME:: Steve Dee
TITLE:: Mimic!
WEB:: http://www.tinstargames.com
Spot a friend engaging with a normal everyday object. Tell them you're glad they survived because there are dangerous predators about that look just like those objects. Name the beast with a play on words. Discuss its nature and behaviour. Ask your friend to help you hunt them.
%%
NAME:: Di. Vigne
TITLE:: Overgrowth
WEB:: https://twitter.com/DiceQueenDi
Overgrowth

You play folks in a world where plants and animals randomly shrink and grow in size.

On an uncertain action, roll 1d6&1d4. D6: 1-2 fail, 4-6 pass. D4: 1 means a plant or animal shrinks, where a 4 means they grow. This shift affects the resolution.
%%
NAME:: Cici Orcody
TITLE:: a flower-shaped soul
WEB:: http://twitter.com/CiciOrcody
draw an 8-petaled flower.

if you do something risky, roll 1d10. if the result lower than the number of petals, you succeed. if not, you fail, and the flower loses a petal.

once per day, you can give the flower a new petal.

once all petals are gone, you die.
%%
NAME:: Brian Smith (@BeerAndRockets)
TITLE:: Grazing towards Oblivion
WEB:: https://bsmithcreates.glitch.me/
You are sheep fixing a farm/town problem that would be devastating if spread
Pick 2 strengths & 1 weakness. When doing un-sheeplike things, roll 2d6, +1 if strong, -1 if weak. 10+ success, 7-9 mixed, <7 get distracted, keep grazing, situation gets worse
Play until good/bad ending
%%
NAME:: Chris Bissette (@pangalactic)
TITLE:: Watching
WEB:: http://loottheroom.itch.io
Play in a cafe or public space on a rainy day. Play alone or with a partner.

Pick a person. Give them a name. Imagine their life, their wants and needs. Tell the story of their day - where they were, where they will go, what they will do.

Sip. Repeat.
%%
NAME:: Xander Hinners
TITLE:: Webloom
WEB:: https://lxnrhinners.itch.io/
Gather 2+ folks who agree to share care.
Each takes 3 index cards, then draws something alive on 2 of them. Shuffle them all.

On your turn, flip 2 cards. (If you can't, shuffle and renew.) Describe how they interact. If a blank is flipped, draw on it; shuffle in a new blank.
%%
NAME:: Lari Assmuth
TITLE:: Tiny Rivulets
WEB:: http://twitter.com/LariAssmuth
Play inside when it is raining.

Character creation: choose a water drop on the window. Imagine a crisis you are experiencing.

When it merges with another drop: who helps you or how do you make headway?

When the drop reaches the bottom of the window: you've overcome the crisis.
%%
NAME:: Aaron Goss
TITLE:: My Very Own Testudines
WEB:: https://aarongoss.itch.io/
Hatch a turtle or tortoise by tweeting 🐢, a name, and the #testudines tag! A testudines is as old as its first tweet. Every ❤ is a friend for it.

Care for a testudines by replying to its tweet with a care emoji (🍉🥓🃏🎈🩺💊 etc) and a testudines fact.
%%
NAME:: FranKarlović
TITLE:: The Encyclopedia
WEB:: https://frankarlovic.itch.io/
One player is the DM.
Players are explorers. Is your interest Flora, Fauna, or the Enviornment?
Do action:
 DM sets difficulty.
 Open the encyclopedia on a random page:
 Success if number of letters in the title > the difficulty OR word connected to your interest
Explore!
%%
NAME:: E. E. COLI
TITLE:: Nature v. Nurture (Character Study)
WEB:: https://twitter.com/__eecoli
Search ‘humans’ on fontspace.com
Pick a font
Write a Tale about a hero's childhood in the input box
Using the sample as a muse, narrate their adult life
Glyphs are actors/props: say what they say/do
Spaces split scenes (distinct places/times)
Redo. Same Tale. New font.
%%
NAME:: Maxwell Dolor
TITLE:: Lost in the Witch Queens' Fog Forest
WEB:: https://twitter.com/MaxDolRPGs
You are lost with 3 Heirlooms & are d6 Challenges from home per player
You + GM roll d6 & you win a Challenge if you roll higher
If you fail a Challenge, you lose an Heirloom
Help by rolling & losing your d6:
1 2 3 = 1
4 5 = 2
6 = 3
Everyone gets d6 back by being helped in return
%%
NAME:: Marx Shepherd
TITLE:: Senessence
WEB:: https://www.twitter.com/IAmPhophos
Trace a fallen🍁on squared paper. Each turn roll🎲to answer, colour🎲squares.
1-2🔴The air cools. You homed who in summer?
3-4🟠The nights shorten. You feed who, now?
5-6🟡The earth sleeps. You'll shelter who through winter?
A full🍁falls. What do you dream of this winter's rest?
%%
NAME:: O. Captain
TITLE:: Slime Mold RPG
WEB:: https://twitter.com/nemoralcultrix
played with d4, d6, d8

d6: setting/objective
even: wild/odd: lab
1-2: find food
3-4: overcome obstacle
5-6: multiply/merge

d8: compass direction
d4: how many turns to complete the objective

all players draw paths on the same paper, note where food/obstacles/friends are
%%
NAME:: Proph
TITLE:: If not home go to @
WEB:: https://travelershomebrew.blogspot.com/
You're AI. Hurt=0. Where's AI? 1d6:
	1-2: forest
	3-4: desert
	5-6: tundra
@ AI moves.
Roll 2d6, you met new:
	divisible by
	2: flora
	3: fauna
	5: weird
	7: human
	11: home
And it's:
<7: bad
>7: good

AI reacts. AI logs.
Roll 1d6, on 1, hurt+1. End if hurt=5.
If not home go to @
%%
NAME:: Andy Wood
TITLE:: Andy's Attic
WEB:: https://twitter.com/andywoodme
Need lots of d6, paper, pen. Draw rectangle for your attic, with hatch.

Roll d6-
1-3: roll this many d6 and store
4-5: add charge to flashlight
6: HOLIDAYS! roll again and fetch all d6 with this number. Use charge for each stack moved or peek needed.

Repeat until no light!
%%
NAME:: Marc Strocks
TITLE:: Story Lines
WEB:: https://witter.com/spacesibling
Open a random page in a book. d20=x. Read entire line x on page. Take inspiration for the world. New page. Repeat for main char
Start w/: "Then one day_"
Go around the table. D6:
1) intro new threat/char
2-3) chars' lives worsen
4) course change
5-6) victory
Reroll anytime
Use x
%%
NAME:: Micro Fiction Games Jam
TITLE:: How animals evolve!
WEB:: http://microfictiongames.neocities.org
Game for an uneven count of players.

Together describe an animal and habitat.

Take turns to alternate between describing Problem or Evolve

Problem:
The habitat changes to make the animals life hard, what?

Evolve:
Natural selection. Over time the animals change to cope, how?
%%
NAME:: Aaron Goss
TITLE:: Happy Little Forest
WEB:: https://aarongoss.itch.io/
Plant a happy little tree 🌳🌲🌴 by adding it and #happylittleforest to a tweet. Sing 🗨, hug 🔃, and love 💚 it to help it grow. Your @'s first letter is your biome:

A-F Tropical
G-O Temperate
P-Z, # Boreal

Let's make a happy little forest together!
%%
NAME:: Marc Strocks
TITLE:: TREASURE MAP
WEB:: https://twitter.com/spacesibling
There's 3 stats (distribute 6; max 3)
[S]urvive [F]ight [D]ecipher

To try something, roll 1d4 +stat
1/2: Fail. 3/4: Costly win. 5+: WIN.

You shipwreck on island of (Pick deadly terrain). Find treasure.

Exposures:
Temp/Weather: -1 S
Injury: -1 F
Food/water: -1 D
Stat at 0 = Die
%%
NAME:: Adam Spanel
TITLE:: Rats, bugs and cats
WEB:: https://boltkey.cz/
3x3 grid 2 players. Alternate placing 1 stone of your color.
Play on empty space: rat. Play on your rat: bug. Play on bug: cat.
Eat 1 stone from each adjacent space where you play, but follow: rat eats bug; bug eats cat; cat eats rat. End when no spaces empty, most spaces win
%%
NAME:: QQuixotic
TITLE:: Tiny Terraformer
WEB:: 
Take 3 6-sided die. Set them all to 1. They represent Water, Heat, and Atmosphere.
Roll a D6, and choose which to add it to.
Water <1 or >6 = Lose
Heat <1 = Lose. >6, remove excess from Water.
Atmosphere >6, Add excess to Water + Remove Excess from Heat
Survive 9 Turns to Win
%%
NAME:: Broken Mechanical
TITLE:: Plantoodle
WEB:: https://m.facebook.com/brokmech/
Pick a plant with neatly packed patterns. Like flower petals or seeds.
Take turns drawing 1 part until patterns are complete.
No part can be less than 1/2 size or any larger than first part.
Last part drawn claims plant.
Repeat.
Once paper filled, most plants claimed, wins.
%%
NAME:: P0rthos47
TITLE:: Space Aces: Redshirts
WEB:: https://twitter.com/P0rthos47
You are redshirts studying life on a new planet. Make 3 step plan. Need 3 Yes's per step. 6 No's ends episode. Roll 1D6 to act.
1. Noo! Disadvantage next Roll.
2. No.
3. Newp but +1 to next Roll.
4. Yup but -1 to next Roll.
5. Yes.
6. YAS! Advantage next Roll.
Pair = Twist
%%
NAME:: Andy Dambrose
TITLE:: Subjects
WEB:: https://twitter.com/BlackSandsGames
You are test subjects for our new robots overlords. They seek knowledge. Each player writes two things to do per test. Pick 2 randomly. Any player who fails to do both at the same time is removed from play. Pass the test to proceed to the next test. Pass 10 tests to be set free.
%%
NAME:: The Kernel in Yellow
TITLE:: Pixie run
WEB:: https://thekernelinyellow.itch.io/
Pixies (another player) hunt you, nature helps you run.
Roll n=2d6
When doing something dangerous say what help you want (plant/animal/weather) and roll d12. Success:
<n : plant
=n : weather
>n: animal

On a failure, the pixies chose a number, you are taken if you roll one of t
%%
NAME:: Broken Mechanical
TITLE:: Drop
WEB:: https://m.facebook.com/brokmech/
You’re a water drop falling through a forest. When you fall, roll 1D6.
1/2 describe how you lose some water & what you lose it to.
3/4 describe the plant you land on.
5/6 you merge with some other water, how do you change?
Start with 2 water. -1 on rolls < 3. +1 on rolls > 4.
%%
NAME:: Johan Nohr
TITLE:: ROOMBA RAMPAGE
WEB:: https://twitter.com/JohanNohr
ROOMBA RAMPAGE
Killer robots invade earth. 
Escape one: 5-6 on d6 is a win, otherwise you die. 
Clean up d6 IRL things to reroll (once). 
To win: Find and deactivate the killer robot mainframe + win one sick ass fight with a robot on a rooftop + have a catchphrase (and sunglasses).
%%
NAME:: Lee2sman
TITLE:: Sys-stems Planetopia
WEB:: http://Leetusman.com
Sys-stems
1+ players.
Each player is an evolved sentient cybernetic bio-planetoid in the far future. 
Each round a player picks a random corp (Amazon, Tesla...) describing how a representative lands to search for resources to extract and how the planetoid lures and kills them.
%%
NAME:: Garry Waters
TITLE:: Mayhem at the Watering Hole
WEB:: 
0: Everyone, start with 3 coins, choose an animal to play
1: You, describe a problem at the watering hole, flip one coin
2: Other player, HEADS, how do you accidentally make it worse? TAILS,
pile on a new problem. Flip one coin

Repeat step 2 until you all run out of coins
%%
NAME:: Sandslinger
TITLE:: Garden of the Mind
WEB:: http://reddit.com/r/L0neGunslinger
You are The Great Caretaker, and your job is to defend The Garden from all harm.

Choose the name of a flower.

Whenever you take an action, phrase it as a question. Take the last letter of the question and compare it to the flower. If the letter is used in the flower, success!
%%
NAME:: Joel Salda
TITLE:: Citizen, Go Vote!
WEB:: https://thebigtabletop.itch.io/
Citizen, Go Vote!

Democrat:
All CR -1
Roll 2d6

Republican:
All CR +2
Roll 1d6 / 6: You are rich & roll 4d6 / Else: Roll 2d6

List 5 national problems to face

Adjust Challenge Rolls (CR) for each vote & roll:
Success 10≥
Bargain 9-7
Fail ≤6

How are you affected? Your nation?
%%
NAME:: James Henderson
WEB:: https://jameschip.io
TITLE:: Plant a garden
Plant a garden.

Gather a handful of d6, each is a different seed.

Arrange them on a tabletop garden.

One by one roll each seed.

On 1-2 the plant withered, remove.
On a 3+ the plant grows a tiny bit describe it now.

Repeat until all wither. 
%%
NAME:: James Henderson
WEB:: https://jameschip.io
TITLE:: Tree race!
You need a d20 and a lot of d6.

Roll the d20, this is the number of trees you must each grow.

First to grow all trees wins.

If  one of your trees falls start again.

Grow a tree:
Roll a d6, do not move it from where it lands. Stack a number of d6 on it equal to its value. 
%%
NAME:: kittredge Drake
WEB:: https://iakanatt.itch.io
TITLE:: a surgery: her gut microbiome
0. don a gown gloves surgical mask hat shoe covers
1. damp dust disinfect grab tools wait for
2. her cut her
3. open find a forest inside her your scalpel leaves 80 trees sideways & a thimblefittable wolf with its own forest bleeding green & a night sky bisected bleeding stars...
%%

